#include <Sample.h> // Sample template

#include "sample_kick.h"
#include "sample_snare.h"
#include "sample_clap.h"
#include "sample_hh.h"
#include "sample_clav.h"
#include "sample_vox.h"

Sample <sample_kick_NUM_CELLS, AUDIO_RATE> sKick(sample_kick_DATA);
Sample <sample_snare_NUM_CELLS, AUDIO_RATE> sSnare(sample_snare_DATA);
Sample <sample_clap_NUM_CELLS, AUDIO_RATE> sClap(sample_clap_DATA);
Sample <sample_hh_NUM_CELLS, AUDIO_RATE> sHiHat(sample_hh_DATA);
Sample <sample_clav_NUM_CELLS, AUDIO_RATE> sClav(sample_clav_DATA);
Sample <sample_vox_NUM_CELLS, AUDIO_RATE> sVox(sample_vox_DATA);

void setup_drum(){
  sKick.setFreq((float) sample_kick_SAMPLERATE / (float) sample_kick_NUM_CELLS);
  sSnare.setFreq((float) sample_snare_SAMPLERATE / (float) sample_snare_NUM_CELLS);
  sClap.setFreq((float) sample_clap_SAMPLERATE / (float) sample_clap_NUM_CELLS);
  sHiHat.setFreq((float) sample_hh_SAMPLERATE / (float) sample_hh_NUM_CELLS);
  sClav.setFreq((float) sample_clav_SAMPLERATE / (float) sample_clav_NUM_CELLS);
  sVox.setFreq((float) sample_vox_SAMPLERATE / (float) sample_vox_NUM_CELLS); 
  LED_ON();  
}

static inline void drum_trigger(int v){
  switch(v/204){
    case 0:
      sKick.start();
      break;
    case 1:
      sSnare.start();
      break;
    case 2:
      sClap.start();
      break;
    case 3:
      sHiHat.start();
      break;
    case 4:
      sClav.start();
      break;
    case 5:
      sVox.start();
      break;
  }
}

int downsample=0;
int downsample_s=0;

bool out = false;
bool vis = false;
bool audio_out=false;
#define LOOP_LENGTH 2*CONTROL_RATE
void control_drum(uint16_t pot, uint16_t ldr, bool key, bool key_old){
  static uint32_t timer = 0u;
  static uint32_t last_event = 0;
  static uint32_t key_clicks[2] = {0,0};
  static bool retrigger = false;
  
  static bool looper = false;
  static uint16_t loop_timer = 0;
  static uint16_t sequence[LOOP_LENGTH]={0};
  
  // prevent sound for first second (ugly but works..)
  if(mozziMicros()>1000000 && audio_out==false) audio_out=true;

  timer++;
  
  // sequence looper
  loop_timer++;
  if(loop_timer==LOOP_LENGTH) loop_timer=0;
  if(sequence[loop_timer]!=0) drum_trigger(sequence[loop_timer]);

  // downsampler
  if(ldr<100) downsample_s=(100-ldr)/3;
  else{
    downsample=0;
    downsample_s=0;
  }
  
  if(looper){
    // rhythmic blinks
    if(loop_timer%(CONTROL_RATE/2)==0) LED_ON();
    if(loop_timer%(CONTROL_RATE/2)==1) LED_OFF();
    // first blink is longer
    if(loop_timer<3) LED_ON();
    if(loop_timer==4) LED_OFF();
  }
  else{
    // visualization
    if(vis) LED_ON();
    else LED_OFF();
  }

  // key press
  if(key && !key_old){
    key_clicks[0]=key_clicks[1];
    key_clicks[1]=timer;
    last_event = timer;
    drum_trigger(pot);
    if(looper) sequence[loop_timer]=pot+1;  // +1 to prevent 0
    if(key_clicks[1]-key_clicks[0] < 2*CONTROL_RATE){
      retrigger=true;
    }
  }
    
  
  if(key && !retrigger && timer-key_clicks[1]>3*CONTROL_RATE){
    if(!looper){
      for(int i=0;i<LOOP_LENGTH;i++) sequence[i]=0;
    }
    key_clicks[1]=timer; // reset
    looper=!looper;
    if(looper) loop_timer=0;
  }
  
  
  // key released
  if(!key){
    retrigger=false;
  }
  
  if(retrigger && timer-last_event==key_clicks[1]-key_clicks[0]){
    if(looper) sequence[loop_timer]=pot+1;  // +1 to prevent 0
    drum_trigger(pot);
    last_event = timer;
  }
}

int s_old=0;
int audio_drum(){
  int s=0;
  s+=sKick.next();
  s+=sSnare.next();
  s+=sClap.next();
  s+=sHiHat.next();
  s+=sClav.next();
  s+=sVox.next();
  
  downsample++;
  if(downsample>=downsample_s){
    downsample=0;
    s_old=s;
  }
  s=s_old;

  vis=s!=0;
  return audio_out?s:0;  
}
