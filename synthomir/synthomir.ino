#include <MozziGuts.h>
#include <mozzi_fixmath.h>
#include <IntMap.h>
#include <RollingAverage.h>

#define CONTROL_RATE 64 // powers of 2 please

#define LED_ON() (PORTD|=(1<<6))
#define LED_OFF() (PORTD&=~(1<<6))

const uint8_t KEY = 4;
const uint8_t POT = A1;
const uint8_t LDR = A2;
const uint8_t LED = 6;

const uint8_t BOOT_MODE = A3;

extern void setup_drum();
extern void control_drum(uint16_t pot, uint16_t ldr, bool key, bool key_old);
extern int audio_drum();
extern void setup_bass();
extern void control_bass(uint16_t pot, uint16_t ldr, bool key, bool key_old);
extern int audio_bass();
extern void setup_ther();
extern void control_ther(uint16_t pot, uint16_t ldr, bool key, bool key_old);
extern int audio_ther();

struct DeviceMode{
  void (*setup)();
  void (*control)(uint16_t, uint16_t, bool, bool);
  int  (*audio)();
};

struct DeviceMode modes[] = {
  { setup_drum, control_drum, audio_drum   }
  ,
  { setup_ther, control_ther, audio_ther   }
  ,
  { setup_bass, control_bass, audio_bass   }
};


// reimplementation of Mozzi's IntMap
struct my_intmap_t{
  int in_min;
  int out_min;
  long multiplier;
};
void my_intmap_init(struct my_intmap_t *s, int in_min, int in_max, int out_min, int out_max){
  s->in_min = in_min;
  s->out_min = out_min;
  s->multiplier = (256L * (out_max-out_min)) / (in_max-in_min);
}
int my_intmap(struct my_intmap_t *s, int n){
  return (int) (((s->multiplier*(n-s->in_min))>>8) + s->out_min);
}

//struct my_intmap_t ldrmap;
const uint16_t LDR_MIN = 200; // covered LDR
uint16_t LDR_MAX = 700;

const IntMap ldrmap(LDR_MIN, LDR_MAX, 512, 0);
RollingAverage<int,4> fLDR;
RollingAverage<int,4> fPOT;


// mapping curve for ldr
static const uint16_t ldr_tbl[502] = {
  200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 200, 201, 201, 201, 201,
  201, 201, 201, 201, 201, 201, 201, 202, 202, 202, 202, 202, 202, 202, 202, 203, 203, 203, 203, 203, 203, 203, 204, 204, 204, 204, 204, 204, 205, 205, 205, 205, 205, 206,
  206, 206, 206, 206, 207, 207, 207, 207, 207, 208, 208, 208, 208, 209, 209, 209, 209, 210, 210, 210, 210, 211, 211, 211, 212, 212, 212, 212, 213, 213, 213, 214, 214, 214,
  215, 215, 215, 216, 216, 216, 217, 217, 217, 218, 218, 218, 219, 219, 220, 220, 220, 221, 221, 222, 222, 222, 223, 223, 224, 224, 224, 225, 225, 226, 226, 227, 227, 228,
  228, 228, 229, 229, 230, 230, 231, 231, 232, 232, 233, 233, 234, 234, 235, 235, 236, 236, 237, 238, 238, 239, 239, 240, 240, 241, 241, 242, 243, 243, 244, 244, 245, 245,
  246, 247, 247, 248, 249, 249, 250, 250, 251, 252, 252, 253, 254, 254, 255, 256, 256, 257, 258, 258, 259, 260, 260, 261, 262, 262, 263, 264, 265, 265, 266, 267, 268, 268,
  269, 270, 271, 271, 272, 273, 274, 274, 275, 276, 277, 278, 278, 279, 280, 281, 282, 282, 283, 284, 285, 286, 287, 288, 288, 289, 290, 291, 292, 293, 294, 294, 295, 296,
  297, 298, 299, 300, 301, 302, 303, 304, 305, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329,
  331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 344, 345, 346, 347, 348, 349, 350, 351, 353, 354, 355, 356, 357, 358, 360, 361, 362, 363, 364, 366, 367, 368,
  369, 370, 372, 373, 374, 375, 377, 378, 379, 380, 382, 383, 384, 386, 387, 388, 389, 391, 392, 393, 395, 396, 397, 399, 400, 401, 403, 404, 405, 407, 408, 409, 411, 412,
  414, 415, 416, 418, 419, 421, 422, 423, 425, 426, 428, 429, 431, 432, 433, 435, 436, 438, 439, 441, 442, 444, 445, 447, 448, 450, 451, 453, 454, 456, 457, 459, 460, 462,
  463, 465, 467, 468, 470, 471, 473, 474, 476, 478, 479, 481, 482, 484, 486, 487, 489, 491, 492, 494, 496, 497, 499, 501, 502, 504, 506, 507, 509, 511, 512, 514, 516, 517,
  519, 521, 523, 524, 526, 528, 530, 531, 533, 535, 537, 538, 540, 542, 544, 546, 547, 549, 551, 553, 555, 556, 558, 560, 562, 564, 566, 568, 569, 571, 573, 575, 577, 579,
  581, 583, 585, 586, 588, 590, 592, 594, 596, 598, 600, 602, 604, 606, 608, 610, 612, 614, 616, 618, 620, 622, 624, 626, 628, 630, 632, 634, 636, 638, 640, 642, 644, 646,
  648, 650, 652, 654, 657, 659, 661, 663, 665, 667, 669, 671, 673, 676, 678, 680, 682, 684, 686, 689, 691, 693, 695, 697, 700, 700
};


// synthomir midi - not tested
//#define SM_MIDI
#ifdef SM_MIDI
#include <MIDI.h>
MIDI_CREATE_DEFAULT_INSTANCE();
#endif

int mode=0;
void setup(){
  pinMode(KEY, INPUT);
  digitalWrite(KEY, HIGH);
  pinMode(LED, OUTPUT);
  pinMode(LDR, INPUT);
  pinMode(POT, INPUT);

  // boot mode
  int bm=analogRead(BOOT_MODE); 
  if(bm<100) mode=2;
  else if(bm>400 && bm<600) mode=1;
  else if(bm>900) mode=0;

  // if key pressed - select mode
  if(digitalRead(KEY)==LOW){
    mode=analogRead(POT)/350;
  }

  for(int i=0; i<mode+1; i++){
    LED_ON();
    delay(200);
    LED_OFF();
    delay(200);
  }
  delay(300);

  modes[mode].setup();

  startMozzi(CONTROL_RATE);

  adcDisconnectAllDigitalIns();
  setupFastAnalogRead();

#ifdef SM_MIDI
  MIDI.begin();
#endif  
//Serial.begin(115200);
}

void loop(){
  audioHook();
}

bool midi_ctl = false;

void updateControl(){
  //uint16_t pot = mozziAnalogRead(POT);
  static uint16_t pot;
  static uint16_t ldr; 
  static bool key; 
  static bool key_old=false;

  
  if(!midi_ctl){
    pot = fPOT.next(mozziAnalogRead(POT));
    ldr = mozziAnalogRead(LDR);
    // FIXE: something's wrong with table
    //ldr = ldrmap(fLDR.next(ldr_tbl[constrain(ldr, LDR_MIN, LDR_MAX-1)-LDR_MIN]));
    ldr = 500-fLDR.next(constrain(ldr, LDR_MIN, LDR_MAX-1)-LDR_MIN);
    key = !digitalRead(KEY);
    
    modes[mode].control(pot, ldr, key, key_old);
    key_old=key;
  }
  
#ifdef SM_MIDI  
  // midi input
  // PC - mode
  // CC10 - pot
  // CC11 - ldr
  // NoteC4 - key
  if(MIDI.read()){
    int m=MIDI.getData1();
    int v;
    switch(MIDI.getType()){
      case midi::ProgramChange:
        if(m<=2) mode=m;
        modes[mode].setup();
      break;
      case midi::ControlChange:
        if(m==10 || m==11){
          v=MIDI.getData2();
          if(m==10){
            pot=v*8;
          }
          else if(m==11){
            ldr=v*(LDR_MAX-LDR_MIN)/128+LDR_MIN;
          }
          midi_ctl=true;
          modes[mode].control(pot, ldr, key, key_old);
        }
      break;
      case midi::NoteOn:
        if(m==60){
          v=MIDI.getData2();
          if(v>0) key=true;
          else key=false;
          modes[mode].control(pot, ldr, key, key_old);          
          key_old=key;
        }
      break;
      case midi::NoteOff:
        if(m==60){
          key=false;
          modes[mode].control(pot, ldr, key, key_old);          
          key_old=key;
        }
      break;
    }
  }
#endif  
  
}

int updateAudio(){
  return modes[mode].audio();
}
