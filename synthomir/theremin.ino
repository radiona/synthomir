#include <Oscil.h>
#include <tables/cos2048_int8.h> // table for Oscils to play
#include <mozzi_utils.h> // for mtof
#include <mozzi_analog.h>
#include <mozzi_midi.h>
#include <mozzi_fixmath.h>
#include <ControlDelay.h>


// sine ther
Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aCos(COS2048_DATA);
Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aCos_dly(COS2048_DATA);
Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aVibrato(COS2048_DATA);


// fm
Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aCarrier(COS2048_DATA);
Oscil<COS2048_NUM_CELLS, AUDIO_RATE> aModulator(COS2048_DATA);
Oscil<COS2048_NUM_CELLS, CONTROL_RATE> kModIndex(COS2048_DATA);
Q8n8 mod_index;// = float_to_Q8n8(2.0f); // constant version
Q16n16 deviation;

Q16n16 carrier_freq, mod_freq;
Q8n8 mod_to_carrier_ratio = float_to_Q8n8(3.f);
Q7n8 target_note, note0, note1, note_upper_limit, note_lower_limit, note_change_step, smoothed_note;

ControlDelay <64, int> kDelayt;

void setup_ther(){
  // sine
  aCos.setFreq(440);
  aCos_dly.setFreq(440);
  aVibrato.setFreq(5.f);
  kDelayt.set(20);

  // fm  
  kModIndex.setFreq(.768f); // sync with kNoteChangeDelay

}


static void ther_setFM(Q7n8 note){
  carrier_freq = Q16n16_mtof(Q8n8_to_Q16n16(note)); // convert midi note to fractional frequency
  mod_freq = ((carrier_freq>>8) * mod_to_carrier_ratio)  ; // (Q16n16>>8)   Q8n8 = Q16n16, beware of overflow
  deviation = ((mod_freq>>16) * mod_index); // (Q16n16>>16)   Q8n8 = Q24n8, beware of overflow
  aCarrier.setFreq_Q16n16(carrier_freq);
  aModulator.setFreq_Q16n16(mod_freq);
}


static uint8_t curr_mode=0;
static int ther_out = 0;
void control_ther(uint16_t pot, uint16_t ldr, bool key, bool key_old){
  int cutoff_freq;
  
  if(key){
    ther_out=1;
    LED_ON();
  }
  else{
    ther_out=0;
    LED_OFF();
  }
  curr_mode=pot/256;
  switch(curr_mode){
    case 0:
      //ldr = map(ldr, 200, 900, 5000, 300);
      // ldr == 0(closed)-512(full light)
      ldr=ldr*8+300;
      aCos.setFreq((float)ldr);
      //aCos_dly.setFreq(kDelayt.next(ldr));
      break;
    case 1:
      mod_to_carrier_ratio = float_to_Q8n8(1.f);
      //ther_setFM(Q7n0_to_Q7n8(map(ldr, 200, 900, 90, 45)));
      ther_setFM(Q7n0_to_Q7n8(ldr/10+45));
      mod_index = (Q8n8)350+kModIndex.next();      
      break;
    case 2:
      mod_to_carrier_ratio = float_to_Q8n8(3.f);
      ther_setFM(Q7n0_to_Q7n8(ldr/10+30));
      mod_index = (Q8n8)350+kModIndex.next();      
      break;
    case 3:
      mod_to_carrier_ratio = float_to_Q8n8(5.5f);
      ther_setFM(Q7n0_to_Q7n8(ldr/10+30));
      mod_index = (Q8n8)350+kModIndex.next();      
      break;
    case 4:
      break;
  }
}

int audio_ther(){
  int8_t asig=0;
  long vibrato;
  switch(curr_mode){
    case 0:
      vibrato = 300 * aVibrato.next();
      asig = (int)aCos.phMod(vibrato);
      //asig=aCos.next()+aCos_dly.next();
      break;
    case 1:
    case 2:
    case 3:
      asig = (int)aCarrier.phMod(deviation * aModulator.next() >> 8);    
      break;
    case 4:
     break;
  }
  return ther_out * asig;
}
