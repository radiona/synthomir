#include <Oscil.h>
#include <tables/saw2048_int8.h> // table for Oscils to play
#include <tables/cos2048_int8.h>
#include <mozzi_utils.h> // for mtof
#include <mozzi_analog.h>
#include <mozzi_midi.h>
#include <mozzi_fixmath.h>
#include <ControlDelay.h>
#include <ADSR.h>


// sine ther
static Oscil<SAW2048_NUM_CELLS, AUDIO_RATE> bass(SAW2048_DATA);
static Oscil<COS2048_NUM_CELLS, AUDIO_RATE> sub(COS2048_DATA);

static ControlDelay <64, int> kDelay;
static ADSR <CONTROL_RATE, AUDIO_RATE> envelope;

//static int32_t notes[9] = {48, 50, 52, 53, 55, 57, 59, 60, 62};
static int32_t notes[9] = {45, 47, 48, 50, 52, 53, 55, 57, 59};

void setup_bass(){
  // sine
  bass.setFreq(220);
  sub.setFreq(110);
  kDelay.set(5);
  envelope.setADLevels(255, 0);
  envelope.setTimes(100, 100000, 0, 200);
}

static int bass_gain = 0;
static int bass_out = 0;

void control_bass(uint16_t pot, uint16_t ldr, bool key, bool key_old){
  
  if(key!=key_old){
    if(key){
      envelope.noteOn();
      bass_out=1;
      LED_ON();
    }
    else{
      envelope.noteOff();
      LED_OFF();
    }
  }

  int dly=5;
  int det=2;
  int attack = 100;
  if(ldr<200){
    dly=5 + 20-ldr/10;
    det=ldr/30;
    attack = (200-ldr)*5+100;
  }
  envelope.setAttackTime(attack);
  kDelay.set(dly);
  
  int f = mtof((int)notes[pot/115]);
  bass.setFreq(f);
  sub.setFreq(kDelay.next(f/2)+det);
  envelope.update();
  bass_out = envelope.playing()?1:0; // envelope doesn't completely shut off (bug?)
}

int audio_bass(){
  int8_t asig=0;
  asig = bass_out*((bass.next()/2+sub.next()/2)*envelope.next())>>8;
  return asig;
}
