# https://forum.arduino.cc/t/merging-bootloader-and-program-into-one-file/87989/8
# Fuses are updated to the one that Arduino shows
echo off
echo ** Write optiboot bootloader on ATmega328P chip
sudo ./avrdude -C./avrdude.conf -c stk500pp -P avrdoper -pm328p -v
echo ** Set fuses - you will need to press yes (y)
sudo ./avrdude -C./avrdude.conf -c stk500pp -P avrdoper  -p m328p -U lfuse:w:0xff:m -U hfuse:w:0xde:m -U efuse:w:0x05:m
echo ** Write bootloader...
sudo ./avrdude -C./avrdude.conf -c stk500pp -P avrdoper -pm328p -U flash:w:optiboot_atmega328.hex
echo ** Lock
sudo ./avrdude -C./avrdude.conf -c stk500pp -P avrdoper -pm328p -U lock:w:0x0f:m
echo ** Bootloader written!
sudo ./avrdude -C./avrdude.conf -c stk500pp -P avrdoper -pm328p -v
echo ** Upload Synthomir sketch...
sudo ./avrdude -C./avrdude.conf -c stk500pp -P avrdoper -pm328p -D -U flash:w:synthomir.hex:i
echo ** Sketch uploaded!