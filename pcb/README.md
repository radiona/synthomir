# Synthomir PCB

Kicad project for Synthomir PCB. Synthomir PCB is open source hardware. 

Schematic and PCB by Igor Brkic <<igor@hyperglitch.com>>. Graphical design and layout by Damir Prizmic <<damir.prizmic@gmail.com>>.

[![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)](http://creativecommons.org/licenses/by-sa/4.0/)      
This work is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/)
